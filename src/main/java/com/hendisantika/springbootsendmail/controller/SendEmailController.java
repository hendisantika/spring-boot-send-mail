package com.hendisantika.springbootsendmail.controller;

import com.hendisantika.springbootsendmail.domain.Email;
import com.hendisantika.springbootsendmail.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-send-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/04/21
 * Time: 07.57
 */
@RestController
public class SendEmailController {

    @Autowired
    private SendEmailService sendEmailService;

    @RequestMapping("/doSend")
    public String doSend() {
        //get data from front-end
        String to = "hendisantika@yahoo.co.id";//if multiple recipients,please use ',' contact
        String subject = "welcome";
        String content = "welcome to Earth";

        Email email = new Email();
        email.setTo(to);
        email.setSubject(subject);
        email.setContent(content);
        //default send email by sendGrid
        String result = sendEmailService.doSendGrid(email);
        if (result.startsWith("2") && !result.equals("200")) {
            return "success";
        } else {
            //if failed, send email by MailGun
            result = sendEmailService.doSendGrid(email);
            return result;
        }
    }


}
