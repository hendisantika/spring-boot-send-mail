package com.hendisantika.springbootsendmail.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-send-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/04/21
 * Time: 07.59
 */
@Log4j2
@RestController
public class HttpMailGun {
    @GetMapping("/sendMailGun")
    public String sendMailGun() {

        String urlStr = "https://api.mailgun.net/v3/sandbox2b80f6a98ddc421eadf1390a8a937b01.mailgun.org/messages";
        String fromStr = "postmaster@sandbox2b80f6a98ddc421eadf1390a8a937b01.mailgun.org";
        String toStr = "hendisantika@yahoo.co.id";
        String subjectStr = "Happy";
        String textStr = "test if you are happy";
        String apiKeyStr = "api:key-e38d18639059d061a4cc4f6eff97f7d1";

        String encodeKey = Base64.getEncoder().encodeToString(apiKeyStr.getBytes());

        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Authorization", "Basic " + encodeKey);
            connection.setRequestProperty("Content-Language", "text/plain");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            OutputStream os = connection.getOutputStream();
            String param = new String();
            param = "from=" + fromStr +
                    "&to=" + toStr +
                    "&subject=" + subjectStr +
                    "&text=" + java.net.URLEncoder.encode(textStr, "GBK");
            os.write(param.getBytes());
            os.flush();
            os.close();

            int responseCode = connection.getResponseCode();
            log.info("Response Code------------------------------------------ : " + responseCode);
            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            log.info("-----" + response.toString());
            rd.close();
            return responseCode + "";
        } catch (Exception e) {
            e.printStackTrace();
            return e + "";
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
