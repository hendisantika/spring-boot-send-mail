package com.hendisantika.springbootsendmail.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-send-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/04/21
 * Time: 08.11
 */
@Log4j2
@RestController
public class HttpSendGrid {
    @GetMapping("/sendSendGrid")
    public void sendSendGrid() {
        String urlStr = "https://api.sendgrid.com/v3/mail/send";
        String fromStr = "hendisantika@gmail.com";
        String toStr = "hendisantika@yahoo.co.id";
        String subjectStr = "Hello";
        String textStr = "Welcome to earth!";
        String apiKeyStr = "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";

        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + apiKeyStr);
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("charset", "UTF-8");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //Send request
            OutputStream os = connection.getOutputStream();
            String param = new String();
            param = "from=" + fromStr +
                    "&to=" + toStr +
                    "&subject=" + subjectStr +
                    "&content=" + java.net.URLEncoder.encode(textStr, "GBK");
            os.write(param.getBytes());
            os.flush();
            os.close();

            int responseCode = connection.getResponseCode();
            log.info("Response Code------------------------------------------ : " + responseCode);

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            log.info("-----" + response.toString());
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
