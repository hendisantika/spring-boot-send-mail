package com.hendisantika.springbootsendmail.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-send-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/04/21
 * Time: 07.32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Email {
    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String content;
}
